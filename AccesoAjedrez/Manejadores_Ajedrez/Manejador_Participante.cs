﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoAjedrez;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Participante
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadesParticipantes part)
        {
            return ca.Comando(string.Format("INSERT INTO Participantes VALUES(" +
           "'{0}','{1}','{2}','{3}')", part.Numerodesocio, part.Nombre, part.Direccion, part.Fkpais));
        }
        public string Modificar(EntidadesParticipantes part)
        {
            return ca.Comando(string.Format("UPDATE Participantes SET nombre='{0}', " + "direccion='{1}', fkidpais='{2}' where num_Socio='{3}'", part.Nombre, part.Direccion, part.Fkpais, part.Numerodesocio));
        }
        public string EliminarDatos(EntidadesParticipantes part)
        {

            return ca.Comando(string.Format("delete from Participantes where num_Socio='{0}'", part.Numerodesocio));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public void LlenarPais(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idPais";
        }
    }
}
