﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bases;
namespace AccesoAjedrez
{
    public class ConexionAjedrez
    {
        Conectar c = new Conectar("localhost", "root", "", "ajedrez1");
        //Método para Insertar, Eliminar, Modificar.
        public string Comando(string q)
        {
            return c.Comando(q);
        }
        //Método para consultar
        public DataSet Mostrar(string q, string tabla)
        {
            return c.Consultar(q, tabla);
        }
    }
}
