﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores_Ajedrez;
using ProyectoAjedrez;


namespace PresentacionAjedrez
{
    public partial class Frm_Sala : Form
    {

        Manejador_Sala ms;
        EntidadesSala es = new EntidadesSala(0,0,0,"");
        string z;
        int fila = 0;
        public Frm_Sala()
        {
            ms = new Manejador_Sala();
            InitializeComponent();
        }
        private void Botonera(Boolean nuevo, Boolean agregar, Boolean eliminar, Boolean modificar, Boolean idHotel, Boolean idSala, Boolean Capacidad, Boolean medio)
        {
            btnNuevo.Enabled = nuevo;
            btnAgregar.Enabled = agregar;
            btnEliminar.Enabled = eliminar;
            btnModificar.Enabled = modificar;
            CmbIdHotel.Enabled = idHotel;
            txtIdSal.Enabled = idSala;
            txtCapacidad.Enabled = Capacidad;
            txtMedio.Enabled = medio;

        }
        void actualizar()
        {
            dtgSala.DataSource = ms.Listado(string.Format("" +
                "select * from Sala"), "Sala").Tables[0];
            dtgSala.AutoResizeColumns();
            dtgSala.Columns[0].ReadOnly = true;

        }
        void limpiar()
        {
            txtIdSal.Clear();
            txtCapacidad.Clear();
            txtMedio.Clear();
            CmbIdHotel.Text = "";
        }

        private void dtgSala_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, false, true, true, true, false, true, true);

            fila = e.RowIndex;
            es.IdHotel = int.Parse(dtgSala.Rows[fila].Cells[0].Value.ToString());
            es.IdSala = int.Parse(dtgSala.Rows[fila].Cells[1].Value.ToString());
            es.Capacidad = int.Parse(dtgSala.Rows[fila].Cells[2].Value.ToString());
            es.Medios = dtgSala.Rows[fila].Cells[3].Value.ToString();
            CmbIdHotel.Text = es.IdHotel.ToString();
            txtIdSal.Text = es.IdSala.ToString();
            txtCapacidad.Text = es.Capacidad.ToString();
            txtMedio.Text = es.Medios;
        }

        private void Frm_Sala_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false, false, false, false, false);
            ms.LlenarHotel(CmbIdHotel, "select idHotel from Hotel", "Hotel");
            actualizar();
            CmbIdHotel.Focus();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, false, false, true, false, true, true);
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                Botonera(true, false, false, false, false, false, false, false);
                string rs = ms.GuardarDatos(new EntidadesSala(int.Parse(CmbIdHotel.Text), es.IdSala , int.Parse(txtCapacidad.Text), txtMedio.Text));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
                limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error Verifica los datos que ingresaste");
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = ms.Modificar(new EntidadesSala (int.Parse(CmbIdHotel.Text) , es.IdSala, int.Parse(txtCapacidad.Text), txtMedio.Text));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            
                DialogResult rs = MessageBox.Show("Atencion, ¿esta seguro de borrar la sala?",
           "Atencion!!!", MessageBoxButtons.YesNo);
                if (rs == DialogResult.Yes)
                {
                    z = ms.EliminarDatos(es);
                    actualizar();
                    limpiar();
                    Botonera(true, false, false, false, false, false, false, false);

                }
            }

        private void CmbIdHotel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}