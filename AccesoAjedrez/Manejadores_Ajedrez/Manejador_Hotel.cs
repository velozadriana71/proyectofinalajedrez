﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoAjedrez;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Hotel
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadesHotel Hotel)
        {
            return ca.Comando(string.Format("INSERT INTO Hotel VALUES(" +
           "null,'{0}','{1}','{2}')", Hotel.Nombre, Hotel.Direccion, Hotel.Telefono));
        }
        public string Modificar(EntidadesHotel Hotel)
        {
            return ca.Comando(string.Format("UPDATE Hotel SET nombre='{0}', " + "direccion='{1}', telefono='{2}' where idHotel='{3}'", Hotel.Nombre, Hotel.Direccion, Hotel.Telefono, Hotel.IdHotel));
        }
        public string EliminarDatos(EntidadesHotel Hotel)
        {

            return ca.Comando(string.Format("delete from Alumno where idHotel='{0}'", Hotel.IdHotel));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }

    }
}
