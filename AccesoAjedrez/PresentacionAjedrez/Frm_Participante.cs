﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores_Ajedrez;
using ProyectoAjedrez;

namespace PresentacionAjedrez
{
    public partial class Frm_Participante : Form
    {
        Manejador_Participante mp;
        EntidadesParticipantes par = new EntidadesParticipantes( 0, "", "",0 );
        string z;
        int fila = 0;
        public Frm_Participante()
        {
            mp = new Manejador_Participante();
            
            InitializeComponent();
        }
        private void Botonera(Boolean nuevo, Boolean agregar, Boolean eliminar, Boolean modificar, Boolean numsocio, Boolean nombre , Boolean direccion, Boolean pais)
        {
            btnNuevo.Enabled = nuevo;
            btnAgregar.Enabled = agregar;
            btnEliminar.Enabled = eliminar;
            btnModificar.Enabled = modificar;
            txtNumSocio.Enabled = numsocio;
            txtNombre.Enabled = nombre;
            txtDireccion.Enabled = direccion;
            cmbPais.Enabled = pais;

            

        }
        void actualizar()
        {
            dtgParticipantes.DataSource = mp.Listado(string.Format("" +
                "select * from Participantes"), "Participantes").Tables[0];
            dtgParticipantes.AutoResizeColumns();
            dtgParticipantes.Columns[0].ReadOnly = true;

        }
        void limpiar()
        {
            txtNumSocio.Clear();
            txtNombre.Clear();
            txtDireccion.Clear();
            cmbPais.Text = "";
        }

        private void Frm_Participante_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false, false, false, false, false);
            mp.LlenarPais (cmbPais, "select idPais from Pais", "Pais");
            actualizar();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, false, false, true, true, true, true);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //Los numero del 1 al 100 son para arbitros y del 101 en adelante son para jugadores
            try
            {
                Botonera(true, false, false, false, false, false, false, false);
                string rs = mp.GuardarDatos(new EntidadesParticipantes(int.Parse(txtNumSocio.Text), txtNombre.Text,txtDireccion.Text,int.Parse(cmbPais.Text)));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
                limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error Verifica los datos que ingresaste");
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mp.Modificar(new EntidadesParticipantes(par.Numerodesocio, txtNombre.Text,txtDireccion.Text, int.Parse(cmbPais.Text)));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, esta seguro de borrar el Pais",
        "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                z = mp.EliminarDatos(par);
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false, false, false);
            }
        }

        private void dtgParticipantes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, false, true, true, false, true, true, true);
            fila = e.RowIndex;
            par.Numerodesocio = int.Parse(dtgParticipantes.Rows[fila].Cells[0].Value.ToString());
            par.Nombre = dtgParticipantes.Rows[fila].Cells[1].Value.ToString();
            par.Direccion = dtgParticipantes.Rows[fila].Cells[2].Value.ToString();
            par.Fkpais = int.Parse(dtgParticipantes.Rows[fila].Cells[3].Value.ToString());
            txtNumSocio.Text = par.Numerodesocio.ToString();
            txtNombre.Text = par.Nombre;
            txtDireccion.Text = par.Direccion;
            cmbPais.Text = par.Fkpais.ToString();
        }

    }
}
