﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores_Ajedrez;
using ProyectoAjedrez;

namespace PresentacionAjedrez
{
    public partial class Frm_Hotel : Form
    {
        Manejador_Hotel mh;
        EntidadesHotel eh = new EntidadesHotel(0,"","","");
        string z;
        int fila = 0;
        public Frm_Hotel()
        {
            mh = new Manejador_Hotel();
            InitializeComponent();
        }
        private void Botonera(Boolean nuevo, Boolean agregar, Boolean eliminar, Boolean modificar, Boolean id, Boolean nombre, Boolean direccion, Boolean telefono)
        {
            btnNuevo.Enabled = nuevo;
            btnAgregar.Enabled = agregar;
            btnEliminar.Enabled = eliminar;
            btnModificar.Enabled = modificar;
            txtId.Enabled = id;
            txtNombre.Enabled = nombre;
            txtDireccion.Enabled = direccion;
            txtTelefono.Enabled = telefono;

        }
        void actualizar()
        {
            dtgHotel.DataSource = mh.Listado(string.Format("" +
                "select * from Hotel"), "Hotel").Tables[0];
            dtgHotel.AutoResizeColumns();
            dtgHotel.Columns[0].ReadOnly = true;

        }
        void limpiar()
        {
            txtId.Clear();
            txtNombre.Clear();
            txtDireccion.Clear();
            txtTelefono.Clear();
        }

        private void Frm_Hotel_Load(object sender, EventArgs e)
        {
            Botonera(true,false,false,false,false,false,false,false);
            actualizar();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, false, false, false, true, true, true);
        }

        private void dtgHotel_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, false, true, true, false, true, true, true);
          
            fila = e.RowIndex;
            eh.IdHotel = int.Parse(dtgHotel.Rows[fila].Cells[0].Value.ToString());
            eh.Nombre = dtgHotel.Rows[fila].Cells[1].Value.ToString();
            eh.Direccion = dtgHotel.Rows[fila].Cells[2].Value.ToString();
            eh.Telefono = dtgHotel.Rows[fila].Cells[3].Value.ToString();
            txtId.Text = eh.IdHotel.ToString();
            txtNombre.Text = eh.Nombre;
            txtDireccion.Text = eh.Direccion;
            txtTelefono.Text = eh.Telefono;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        { 
            try
            {
                Botonera(true, false, false, false, false, false, false, false);
                string rs = mh.GuardarDatos(new EntidadesHotel(eh.IdHotel, txtNombre.Text, txtDireccion.Text, txtTelefono.Text));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
                limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error Verifica los datos que ingresaste");
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mh.Modificar(new EntidadesHotel(eh.IdHotel, txtNombre.Text, txtDireccion.Text, txtTelefono.Text));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, ¿esta seguro de borrar el Hotel?",
        "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                z = mh.EliminarDatos(eh);
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false, false, false);
            }
        }
    }
}
