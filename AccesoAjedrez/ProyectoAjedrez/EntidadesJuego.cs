﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoAjedrez
{
    public class EntidadesJuego
    {
        public int IdJuego { get; set; }
        public int IDJugador { get; set; }
        public string Color { get; set; }

        public EntidadesJuego(int idjuego,int idjugador,string color )
        {
            IdJuego = idjuego;
            IDJugador = idjugador;
            Color = color;

        }
    }
}
