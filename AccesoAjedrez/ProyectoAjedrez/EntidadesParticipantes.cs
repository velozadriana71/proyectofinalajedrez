﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoAjedrez
{
    public class EntidadesParticipantes
    {
        public int Numerodesocio { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public int Fkpais { get; set; }
        public EntidadesParticipantes(int numerosocio,string nombre,string direccion,int fkpais)
        {
            Numerodesocio = numerosocio;
            Nombre = nombre;
            Direccion = direccion;
            Fkpais = fkpais;

        }

    }
}
