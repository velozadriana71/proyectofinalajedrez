﻿using Manejadores_Ajedrez;
using ProyectoAjedrez;
using System;
using System.Windows.Forms;

namespace PresentacionAjedrez
{

    public partial class FrmArbitro : Form
    {
        Manejador_Arbitro mar;
        EntidadArbitro ear = new EntidadArbitro(0);
        int fila = 0;
        string z;
        public FrmArbitro()
        {
            InitializeComponent();
            mar = new Manejador_Arbitro();
            
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, false, true);
            
            cmbId.Focus();
        }

        private void Botonera(Boolean nuevo, Boolean guardar, Boolean eliminar, Boolean id)
        {
            BtnNuevo.Enabled = nuevo;
            BtnAgregar.Enabled = guardar;
            BtnEliminar.Enabled = eliminar;
            cmbId.Enabled = id;
        }
        

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
               
                string rs = mar.GuardarDatos(new EntidadArbitro(int.Parse(cmbId.Text)));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
                cmbId.Text = "";
                Botonera(true, false, false, false);

            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }
        void actualizar()
        {
            DtgArbitro.DataSource = mar.Listado(string.Format("" +
                "select * from Arbitro"), "Arbitro").Tables[0];
            DtgArbitro.AutoResizeColumns();
            DtgArbitro.Columns[0].ReadOnly = true;

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, esta seguro de borrar el Arbitro",
          "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                z = mar.EliminarDatos(ear);
                actualizar();
                cmbId.Text = "";
                Botonera(true, false, false, false);
            }
        }

        private void FrmArbitro_Load(object sender, EventArgs e)
        {
            Botonera(true,false,false,false);
            actualizar();
            mar.LlenarArbitro(cmbId, "select num_Socio from Participantes where num_Socio <=100", "Participantes"); //Los id menores o igual a 100 son para arbitros
        }

        private void DtgArbitro_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, false, true, false);
            fila = e.RowIndex;
            ear.IdArbitro = int.Parse(DtgArbitro.Rows[fila].Cells[0].Value.ToString());
            cmbId.Text = ear.IdArbitro.ToString();
        }

        private void cmbId_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
