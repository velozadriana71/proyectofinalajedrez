﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores_Ajedrez;
using ProyectoAjedrez;

namespace PresentacionAjedrez
{
    public partial class Frm_Jugador : Form
    {
        Manejador_Jugador mj;
        EntidadesJugador Jug = new EntidadesJugador(0,"");
        string z;
        int fila = 0;
        public Frm_Jugador()
        {
            mj = new Manejador_Jugador();
            InitializeComponent();
        }
        private void Botonera(Boolean nuevo, Boolean agregar, Boolean eliminar, Boolean modificar, Boolean id, Boolean nivel)
        {
            btnNuevo.Enabled = nuevo;
            btnAgregar.Enabled = agregar;
            btnEliminar.Enabled = eliminar;
            btnModificar.Enabled = modificar;
            cmbId.Enabled = id;
            txtnivel.Enabled = nivel;

            
        }
        void actualizar()
        {
            dtgParticipantes.DataSource = mj.Listado(string.Format("" +
                "select * from Jugador"), "Jugador").Tables[0];
            dtgParticipantes.AutoResizeColumns();
            dtgParticipantes.Columns[0].ReadOnly = true;

        }
        void limpiar()
        {
            cmbId.Text = "";
            txtnivel.Clear();
        }

        private void Frm_Jugador_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false, false, false);
            mj.LlenarJugador(cmbId, "select num_Socio from Participantes where num_Socio > 100", "Participantes"); //Los id mayores a 100 son para jugadore
            actualizar();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, false, false, true, true);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
           
            try
            {
                
                string rs = mj.GuardarDatos(new EntidadesJugador(int.Parse(cmbId.Text), txtnivel.Text));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error Verifica los datos que ingresaste");
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mj.Modificar(new EntidadesJugador(Jug.IdJugador, txtnivel.Text));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, ¿esta seguro de borrar el jugador seleccionado?",
       "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                z = mj.EliminarDatos(Jug);
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false);
            }
        }

        private void dtgParticipantes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, false, true, true, false, true);
            fila = e.RowIndex;
            Jug.IdJugador = int.Parse(dtgParticipantes.Rows[fila].Cells[0].Value.ToString());
            Jug.Nivel = dtgParticipantes.Rows[fila].Cells[1].Value.ToString();
            cmbId.Text = Jug.IdJugador.ToString();
            txtnivel.Text = Jug.Nivel;
        }
    }
}
