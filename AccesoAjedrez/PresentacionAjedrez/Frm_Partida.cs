﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores_Ajedrez;
using ProyectoAjedrez;

namespace PresentacionAjedrez
{
    public partial class Frm_Partida : Form
    {
        Manejador_Partida mp1;
        EntidadesPartida enp = new EntidadesPartida(0, "", "", 0, 0, 0, 0);
        int fila = 0;
        string z = "";
        public Frm_Partida()
        {

            InitializeComponent();
            mp1 = new Manejador_Partida();
        }

        private void Botonera(Boolean nuevo, Boolean agregar, Boolean modificar, Boolean eliminar,Boolean idp,Boolean fce,Boolean ho,Boolean fkar,Boolean fkho,Boolean fksa,Boolean entrad)
        {
            btnnuevo.Enabled = nuevo;
            btnagregar.Enabled = agregar;
            btnmodificar.Enabled = modificar;
            btnborrar.Enabled = eliminar;
            txtidpartida.Enabled = idp;
            dtfecha.Enabled = fce;
            txthora.Enabled = ho;
            cmbarbitro.Enabled = fkar;
            cmbhotel.Enabled = fkho;
            cmbsala.Enabled = fksa;
            txtentradas.Enabled = entrad;
        }


        void limpiar()
        {
            txtidpartida.Clear();
            dtfecha.Text = "";
            txthora.Clear();
            cmbarbitro.Text = "";
            cmbhotel.Text = "";
            cmbsala.Text = "";
            txtentradas.Clear();

        }
        private void Frm_Partida_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false,false, false, false, false, false, false, false);
            mp1.LlenarArbitro(cmbarbitro, "select idArbitro from Arbitro", "Arbitro");
            mp1.LlenarHotel(cmbhotel, "select idHotel from Hotel", "Hotel");
            mp1.LlenarSala(cmbsala, "Select idsala from sala", "Sala");

            dtfecha.Format = DateTimePickerFormat.Custom;
            dtfecha.CustomFormat = "yyyy-MM-dd";

            actualizar();

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(true, false, true, true, true, true, true, true, true, true, true);

            fila = e.RowIndex;
            enp.Idpartida = int.Parse(DtgPartida.Rows[fila].Cells[0].Value.ToString());
            enp.JornadaFec = DtgPartida.Rows[fila].Cells[1].Value.ToString();
            enp.JornadaHora = DtgPartida.Rows[fila].Cells[2].Value.ToString();
            enp.FkArbitro = int.Parse(DtgPartida.Rows[fila].Cells[3].Value.ToString());
            enp.FkIdhotel = int.Parse(DtgPartida.Rows[fila].Cells[4].Value.ToString());
            enp.FkidSala = int.Parse(DtgPartida.Rows[fila].Cells[5].Value.ToString());
            enp.Entradas = int.Parse(DtgPartida.Rows[fila].Cells[6].Value.ToString());

            txtidpartida.Text = enp.Idpartida.ToString();
            dtfecha.Text = enp.JornadaFec;
            txthora.Text = enp.JornadaHora;
            cmbarbitro.Text = enp.FkArbitro.ToString();
            cmbhotel.Text = enp.FkIdhotel.ToString();
            cmbsala.Text = enp.FkidSala.ToString();
            txtentradas.Text = enp.Entradas.ToString();

        }

        private void btnnuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, false, false, true, true, true, true, true, true, true);

        }

        private void btnagregar_Click(object sender, EventArgs e)
        {
            try
            {
                Botonera(false, true, false, false, true, true, true, true, true, true, true);
                string rs = mp1.GuardarDatos(new EntidadesPartida(0, dtfecha.Text, txthora.Text, int.Parse(cmbarbitro.Text), int.Parse(cmbhotel.Text), int.Parse(cmbsala.Text), int.Parse(txtentradas.Text)));
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
                //limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error Verifica los datos que ingresaste");
            }
        }
        void actualizar()
        {
            DtgPartida.DataSource = mp1.Listado(string.Format("" +
                "select * from Partida"), "Partida").Tables[0];
            DtgPartida.AutoResizeColumns();
            DtgPartida.Columns[0].ReadOnly = true;

        }

        private void btnmodificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mp1.Modificar(new EntidadesPartida(enp.Idpartida, dtfecha.Text," ",0,0,0,0));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false, false, false, false, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void btnborrar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, ¿esta seguro de borrar la Partida",
           "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                z = mp1.Borrar(enp);
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false, false, false, false, false, false);

            }
        }
    }
}
