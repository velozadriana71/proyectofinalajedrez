﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores_Ajedrez;
using ProyectoAjedrez;

namespace PresentacionAjedrez
{
    public partial class Frm_Juego : Form
    {
        Manejador_Juego mnj;
        EntidadesJuego entj = new EntidadesJuego(0, 0, "");
        string z;
        int fila = 0;
        public Frm_Juego()
        {
            InitializeComponent();
            mnj = new Manejador_Juego();

        }
        private void Botonera(Boolean nuevo, Boolean agregar,  Boolean modificar, Boolean eliminar,Boolean cbpar,Boolean cbjuga,Boolean tcolor)
        {
            btnNuevo.Enabled = nuevo;
            btnagregar.Enabled = agregar;
            btnmodificar.Enabled = eliminar;
            btnBorra.Enabled = modificar;
            cmbidparit.Enabled = cbpar;
            cmidjugador.Enabled = cbjuga;
            txtcolor.Enabled = tcolor;
        }
        void actualizar()
        {
            dataGridView1.DataSource = mnj.Listado(string.Format("" +
                "select * from juego"), "Juego").Tables[0];
            dataGridView1.AutoResizeColumns();
            dataGridView1.Columns[0].ReadOnly = true;

        }
        void limpiar()
        {
            cmbidparit.Text = "";
            cmidjugador.Text = "";
            txtcolor.Clear();
        }

        private void Frm_Juego_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false,false,false,false);
            
            mnj.LlenarJugador(cmidjugador, "select idjugador from jugador ", "jugador");
            mnj.LlenarPartida(cmbidparit, "select idpartida from partida", "partida");
            actualizar();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, false, false,true,true,true);
        }

        private void btnagregar_Click(object sender, EventArgs e)
        {
            try
            {

                string rs = mnj.GuardarDatos(new EntidadesJuego(int.Parse(cmbidparit.Text), int .Parse(cmidjugador.Text),txtcolor.Text));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
                limpiar();
                Botonera(true, false, false, false,false,false,false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error Verifica los datos que ingresaste");
            }
        }

        private void btnmodificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mnj.Modificar(new EntidadesJuego(entj.IdJuego,0,txtcolor.Text));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
                limpiar();
                Botonera(true, false, false, false,true,true,true);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void btnBorra_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, ¿esta seguro de borrar el seleccionado?",
       "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                z = mnj.EliminarDatos(entj);
                actualizar();
                limpiar();
                Botonera(true, false, false, false,false,false,false);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(true, false, true, true,true,  true,true);
            fila = e.RowIndex;
            entj.IdJuego = int.Parse(dataGridView1.Rows[fila].Cells[0].Value.ToString());
            entj.IDJugador = int.Parse(dataGridView1.Rows[fila].Cells[1].Value.ToString());
            entj.Color = dataGridView1.Rows[fila].Cells[2].Value.ToString();
            cmbidparit.Text = entj.IdJuego.ToString();
            cmidjugador.Text = entj.IDJugador.ToString();
            txtcolor.Text = entj.Color;
        }
    }
}
