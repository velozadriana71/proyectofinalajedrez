﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores_Ajedrez;
using ProyectoAjedrez;

namespace PresentacionAjedrez
{
    public partial class FrmPais : Form
    {
        Manejador_Pais mp;
        EntidadesPais ep = new EntidadesPais(0, "", "", "");
        string z;
        int fila = 0;
        public FrmPais()
        {
            InitializeComponent();
            mp = new Manejador_Pais();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Botonera(false, true, false, false, false, true, true, true);
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
               
                string rs = mp.GuardarDatos(new EntidadesPais(ep.Idpais, TxtNombre.Text, TxtClub.Text, TxtRepresentante.Text));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                Botonera(true, false, false, false, false, false, false, false);
                actualizar();

            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }
        void actualizar()
        {
            DtgPais.DataSource = mp.Listado(string.Format("" +
                "select * from Pais"), "Pais").Tables[0];
            DtgPais.AutoResizeColumns();
            DtgPais.Columns[0].ReadOnly = true;

        }
        private void Botonera(Boolean nuevo, Boolean agregar, Boolean eliminar, Boolean modificar, Boolean id, Boolean nombre, Boolean club, Boolean representante )
        {
            BtnNuevo.Enabled = nuevo;
            BtnAgregar.Enabled = agregar;
            BtnEliminar.Enabled = eliminar;
            BtnModificar.Enabled = modificar;
            TxtId.Enabled = id;
            TxtNombre.Enabled = nombre;
            TxtClub.Enabled = club;
            TxtRepresentante.Enabled = representante;

          
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, esta seguro de borrar el Pais",
         "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                z = mp.EliminarDatos(ep);
                Botonera(true, false, false, false, false, false, false, false);
                actualizar();
            }
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mp.Modificar(new EntidadesPais(ep.Idpais,TxtNombre.Text,TxtClub.Text,TxtRepresentante.Text));
                MessageBox.Show("Se Actualizo Correctamente");
                Botonera(true, false, false, false, false, false, false, false);
                actualizar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void FrmPais_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false, false, false, false, false);
            actualizar();
        }

        private void DtgPais_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, false, true, true, false, true, true, true);
            fila = e.RowIndex;
            ep.Idpais = int.Parse(DtgPais.Rows[fila].Cells[0].Value.ToString());
            ep.Nombre = DtgPais.Rows[fila].Cells[1].Value.ToString();
            ep.NumerodeClub = DtgPais.Rows[fila].Cells[2].Value.ToString();
            ep.Representa = DtgPais.Rows[fila].Cells[3].Value.ToString();
            TxtId.Text = ep.Idpais.ToString();
            TxtNombre.Text = ep.Nombre;
            TxtClub.Text = ep.NumerodeClub;
            TxtRepresentante.Text = ep.Representa;
        }
    }
}
