﻿using System.Data;
using System.Windows.Forms;
using AccesoAjedrez;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Sala
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadesSala sala)
        {
            return ca.Comando(string.Format("INSERT INTO Sala VALUES(" +
           "'{0}',null,'{1}','{2}')", sala.IdHotel, sala.Capacidad, sala.Medios));
        }
        public string Modificar(EntidadesSala sala)
        {
            return ca.Comando(string.Format("UPDATE Sala SET capacidad='{0}'," + "medios='{1}' where idSala='{2}'", sala.Capacidad, sala.Medios, sala.IdSala));
        }
        public string EliminarDatos(EntidadesSala sala)
        {
            return ca.Comando(string.Format("delete from Sala where idSala='{0}'", sala.IdSala));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public void LlenarHotel(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idHotel";
        }
    }
}
