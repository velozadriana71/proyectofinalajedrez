﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoAjedrez;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Jugador
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadesJugador Jugador)
        {
            return ca.Comando(string.Format("INSERT INTO Jugador VALUES(" +
           "{0}, '{1}')", Jugador.IdJugador, Jugador.Nivel));

        }
        public string Modificar(EntidadesJugador Jugador)
        {
 
            return ca.Comando(string.Format("UPDATE Jugador SET nivel='{0}'  where idJugador='{1}'", Jugador.Nivel, Jugador.IdJugador));

           
        }
        public string EliminarDatos(EntidadesJugador Jugador)
        {
            return ca.Comando(string.Format("DELETE FROM Jugador WHERE idArbitro='{0}'", Jugador.IdJugador));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public void LlenarJugador(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "num_Socio";
        }
    }
}
