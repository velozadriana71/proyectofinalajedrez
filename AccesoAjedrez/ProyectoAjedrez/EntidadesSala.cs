﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoAjedrez
{
   public class EntidadesSala
    {
        public int IdHotel { get; set; }
        public int IdSala { get; set; }
        public int Capacidad { get; set; }
        public string Medios { get; set; }

        public EntidadesSala(int idhotel,int idsala,int capacidad,string medios)
        {
            IdHotel = idhotel;
            IdSala = idsala;
            Capacidad = capacidad;
            Medios = medios;
        }


    }
}
