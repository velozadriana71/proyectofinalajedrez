﻿namespace PresentacionAjedrez
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.tsHotel = new System.Windows.Forms.ToolStripButton();
            this.tsCampeonato = new System.Windows.Forms.ToolStripButton();
            this.tsSala = new System.Windows.Forms.ToolStripButton();
            this.tsPartida = new System.Windows.Forms.ToolStripButton();
            this.tsJuego = new System.Windows.Forms.ToolStripButton();
            this.tsMovimientos = new System.Windows.Forms.ToolStripButton();
            this.tsReserva = new System.Windows.Forms.ToolStripButton();
            this.tsSalir = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton4,
            this.tsHotel,
            this.tsCampeonato,
            this.tsSala,
            this.tsPartida,
            this.tsJuego,
            this.tsMovimientos,
            this.tsReserva,
            this.tsSalir});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(841, 71);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(68, 68);
            this.toolStripButton1.Text = "Pais";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(68, 68);
            this.toolStripButton2.Text = "Participante";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(68, 68);
            this.toolStripButton3.Text = "Arbitro";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(68, 68);
            this.toolStripButton4.Text = "Jugador";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // tsHotel
            // 
            this.tsHotel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsHotel.Image = ((System.Drawing.Image)(resources.GetObject("tsHotel.Image")));
            this.tsHotel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsHotel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsHotel.Name = "tsHotel";
            this.tsHotel.Size = new System.Drawing.Size(68, 68);
            this.tsHotel.Text = "Hotel";
            this.tsHotel.Click += new System.EventHandler(this.tsHotel_Click);
            // 
            // tsCampeonato
            // 
            this.tsCampeonato.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsCampeonato.Image = ((System.Drawing.Image)(resources.GetObject("tsCampeonato.Image")));
            this.tsCampeonato.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsCampeonato.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsCampeonato.Name = "tsCampeonato";
            this.tsCampeonato.Size = new System.Drawing.Size(68, 68);
            this.tsCampeonato.Text = "Campeonato";
            this.tsCampeonato.Click += new System.EventHandler(this.tsCampeonato_Click);
            // 
            // tsSala
            // 
            this.tsSala.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsSala.Image = ((System.Drawing.Image)(resources.GetObject("tsSala.Image")));
            this.tsSala.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsSala.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSala.Name = "tsSala";
            this.tsSala.Size = new System.Drawing.Size(68, 68);
            this.tsSala.Text = "Sala";
            this.tsSala.Click += new System.EventHandler(this.tsSala_Click);
            // 
            // tsPartida
            // 
            this.tsPartida.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsPartida.Image = ((System.Drawing.Image)(resources.GetObject("tsPartida.Image")));
            this.tsPartida.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsPartida.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsPartida.Name = "tsPartida";
            this.tsPartida.Size = new System.Drawing.Size(68, 68);
            this.tsPartida.Text = "Partida";
            this.tsPartida.Click += new System.EventHandler(this.tsPartida_Click);
            // 
            // tsJuego
            // 
            this.tsJuego.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsJuego.Image = ((System.Drawing.Image)(resources.GetObject("tsJuego.Image")));
            this.tsJuego.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsJuego.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsJuego.Name = "tsJuego";
            this.tsJuego.Size = new System.Drawing.Size(68, 68);
            this.tsJuego.Text = "Juego";
            this.tsJuego.Click += new System.EventHandler(this.tsJuego_Click);
            // 
            // tsMovimientos
            // 
            this.tsMovimientos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsMovimientos.Image = ((System.Drawing.Image)(resources.GetObject("tsMovimientos.Image")));
            this.tsMovimientos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsMovimientos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsMovimientos.Name = "tsMovimientos";
            this.tsMovimientos.Size = new System.Drawing.Size(68, 68);
            this.tsMovimientos.Text = "Movimientos";
            this.tsMovimientos.Click += new System.EventHandler(this.tsMovimientos_Click);
            // 
            // tsReserva
            // 
            this.tsReserva.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsReserva.Image = ((System.Drawing.Image)(resources.GetObject("tsReserva.Image")));
            this.tsReserva.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsReserva.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsReserva.Name = "tsReserva";
            this.tsReserva.Size = new System.Drawing.Size(68, 68);
            this.tsReserva.Text = "Reserva";
            this.tsReserva.Click += new System.EventHandler(this.tsReserva_Click);
            // 
            // tsSalir
            // 
            this.tsSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsSalir.Image = ((System.Drawing.Image)(resources.GetObject("tsSalir.Image")));
            this.tsSalir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSalir.Name = "tsSalir";
            this.tsSalir.Size = new System.Drawing.Size(68, 68);
            this.tsSalir.Text = "Salir";
            this.tsSalir.Click += new System.EventHandler(this.tsSalir_Click);
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 476);
            this.ControlBox = false;
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.Name = "FrmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MENU AJEDREZ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton tsHotel;
        private System.Windows.Forms.ToolStripButton tsCampeonato;
        private System.Windows.Forms.ToolStripButton tsSala;
        private System.Windows.Forms.ToolStripButton tsPartida;
        private System.Windows.Forms.ToolStripButton tsJuego;
        private System.Windows.Forms.ToolStripButton tsMovimientos;
        private System.Windows.Forms.ToolStripButton tsReserva;
        private System.Windows.Forms.ToolStripButton tsSalir;
    }
}