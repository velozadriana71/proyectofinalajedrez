﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoAjedrez;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Juego
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadesJuego Juego)
        {
            return ca.Comando(string.Format("INSERT INTO Juego VALUES(" +
           "'{0}','{1}','{2}')", Juego.IdJuego, Juego.IDJugador, Juego.Color));
        }
        public string Modificar(EntidadesJuego Juego)
        {
            return ca.Comando(string.Format("UPDATE Juego SET color='{0}' WHERE idPartida='{1}'", Juego.Color, Juego.IdJuego));
        }
        public string EliminarDatos(EntidadesJuego Juego)
        {

            return ca.Comando(string.Format("delete from Juego where Color='{0}'", Juego.Color));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public void LlenarPartida(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idPartida";
        }
        public void LlenarJugador(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idJugador";
        }
    }
}
