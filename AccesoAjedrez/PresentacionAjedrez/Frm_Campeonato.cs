﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores_Ajedrez;
using ProyectoAjedrez;

namespace PresentacionAjedrez
{
    public partial class Frm_Campeonato : Form
    {
        Manejador_Campeonato mc;
        EntidadesCampeonato ec = new EntidadesCampeonato(0,"","",0);
        string z;
        int fila = 0;
        public Frm_Campeonato()
        {
            mc = new Manejador_Campeonato();
            InitializeComponent();
        }
        private void Botonera(Boolean nuevo, Boolean agregar, Boolean eliminar, Boolean modificar, Boolean id, Boolean nombre, Boolean tipo, Boolean idpartida)
        {
            btnNuevo.Enabled = nuevo;
            btnAgregar.Enabled = agregar;
            btnEliminar.Enabled = eliminar;
            btnModificar.Enabled = modificar;
            txtId.Enabled = id;
            txtNombre.Enabled = nombre;
            txtTipo.Enabled = tipo;
            CmbIdPart.Enabled = idpartida;

        }
        void actualizar()
        {
            dtgCampeonato.DataSource = mc.Listado(string.Format("" +
                "select * from Campeonato"), "Campeonato").Tables[0];
            dtgCampeonato.AutoResizeColumns();
            dtgCampeonato.Columns[0].ReadOnly = true;

        }
        void limpiar()
        {
            txtId.Clear();
            txtNombre.Clear();
            txtTipo.Clear();
            CmbIdPart.Text = "";
        }

        private void dtgCampeonato_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(false, false, true, true, false, true, true, true);

            fila = e.RowIndex;
            ec.IdCampeonato = int.Parse(dtgCampeonato.Rows[fila].Cells[0].Value.ToString());
            ec.Nombre= dtgCampeonato.Rows[fila].Cells[1].Value.ToString();
            ec.Tipo = dtgCampeonato.Rows[fila].Cells[2].Value.ToString();
            ec.Fkidparticipante = int.Parse( dtgCampeonato.Rows[fila].Cells[3].Value.ToString());
            txtId.Text = ec.IdCampeonato.ToString();
            txtNombre.Text = ec.Nombre;
            txtTipo.Text = ec.Tipo;
            CmbIdPart.Text = ec.Fkidparticipante.ToString();
        }
        private void Frm_Campeonato_Load(object sender, EventArgs e)
        {
            Botonera(true,false,false,false,false,false,false,false);
            mc.LlenarAlumnos(CmbIdPart, "select num_Socio from Participantes", "Participantes");
            actualizar();
            txtNombre.Focus();

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false,true,false,false,false,true,true,true);
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                Botonera(true, false, false, false, false, false, false, false);
                string rs = mc.GuardarDatos(new EntidadesCampeonato(ec.IdCampeonato, txtNombre.Text, txtTipo.Text, int.Parse(CmbIdPart.Text)));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
                limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show("Error Verifica los datos que ingresaste");
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mc.Modificar(new EntidadesCampeonato(ec.IdCampeonato, txtNombre.Text, txtTipo.Text, int.Parse(CmbIdPart.Text)));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, ¿esta seguro de borrar el campeonato?",
       "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                z = mc.EliminarDatos(ec);
                actualizar();
                limpiar();
                Botonera(true, false, false, false, false, false, false, false);

            }
        }
    }
}
