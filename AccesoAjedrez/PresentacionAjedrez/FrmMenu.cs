﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores_Ajedrez;

namespace PresentacionAjedrez
{
    public partial class FrmMenu : Form
    {
        public FrmMenu()
        {
            InitializeComponent();
        }

       

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
           FrmPais p = new FrmPais();
            p.MdiParent = this;  //PARA MANDAR A LLAMAR A UN EVENTO
            p.Show();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Frm_Participante p = new Frm_Participante();
            p.MdiParent = this;
            p.Show();
        }
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            FrmArbitro per = new FrmArbitro();
            per.MdiParent = this;  //PARA MANDAR A LLAMAR A UN EVENTO
            per.Show();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            Frm_Jugador p = new Frm_Jugador();
            p.MdiParent = this;  //PARA MANDAR A LLAMAR A UN EVENTO
            p.Show();
        }

        private void tsHotel_Click(object sender, EventArgs e)
        {
            Frm_Hotel p = new Frm_Hotel();
            p.MdiParent = this;  //PARA MANDAR A LLAMAR A UN EVENTO
            p.Show();
        }

        private void tsSalir_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, ¿sta seguro de cerrar el programa?",
          "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                Application.Exit();
                
            }
        }

        private void tsCampeonato_Click(object sender, EventArgs e)
        {
            Frm_Campeonato p = new Frm_Campeonato();
            p.MdiParent = this;  //PARA MANDAR A LLAMAR A UN EVENTO
            p.Show();
        }

        private void tsSala_Click(object sender, EventArgs e)
        {
            Frm_Sala p = new Frm_Sala();
            p.MdiParent = this;  //PARA MANDAR A LLAMAR A UN EVENTO
            p.Show();
        }

        private void tsPartida_Click(object sender, EventArgs e)
        {
            Frm_Partida parida = new Frm_Partida();
             parida.MdiParent = this;
            parida.Show();
        }

        private void tsJuego_Click(object sender, EventArgs e)
        {
            Frm_Juego jueg = new Frm_Juego();
            jueg.MdiParent = this;
            jueg.Show();
        }

        private void tsMovimientos_Click(object sender, EventArgs e)
        {
           FrmMovimientos mov = new FrmMovimientos();
            mov.MdiParent = this;
            mov.Show();
        }

        private void tsReserva_Click(object sender, EventArgs e)
        {
            FrmReserva res = new FrmReserva();
            res.MdiParent = this;
            res.Show();
        }
    }
}
