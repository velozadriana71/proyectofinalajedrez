﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoAjedrez;
using Manejadores_Ajedrez;
namespace PresentacionAjedrez
{
    public partial class FrmMovimientos : Form
    {
        Manejador_Movimiento mM;
        EntidadMovimiento Mov = new EntidadMovimiento(0, 0, "", "");
        string t;
        int fila = 0;
        public FrmMovimientos()
        {
            InitializeComponent();
            mM = new Manejador_Movimiento();
        }
        private void Botonera(Boolean nuevo, Boolean agregar, Boolean eliminar, Boolean modificar, Boolean idpartida, Boolean idmovimiento, Boolean movimento, Boolean comentario)
        {
            btnNuevo.Enabled = nuevo;
            btnAgregar.Enabled = agregar;
            btnEliminar.Enabled = eliminar;
            btnModificar.Enabled = modificar;
            cmbIdP.Enabled = idpartida;
            txtIdMovimiento.Enabled = idmovimiento;
            txtMovimiento.Enabled = movimento;
            txtComentario.Enabled = comentario;


        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, false, false, true, true, true, true);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {

                string rs = mM.GuardarDatos(new EntidadMovimiento(int.Parse(cmbIdP.Text), 0, txtMovimiento.Text, txtComentario.Text));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
                //limpiar();
                Botonera(true, false, false, false, false, false, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error Verifica los datos que ingresaste");
            }
        }
        void actualizar()
        {
            dtgMovimientos.DataSource = mM.Listado(string.Format("" + "select * from Movimientos"), "Movimientos").Tables[0];
            dtgMovimientos.AutoResizeColumns();
            dtgMovimientos.Columns[0].ReadOnly = true;

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, ¿esta seguro de borrar el Movimiento seleccionado?",
             "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                t = mM.EliminarDatos(Mov);
                actualizar();
                //limpiar();
                Botonera(true, false, false, false, false, false, false, false);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mM.Modificar(new EntidadMovimiento(0, Mov.IdMovimiento, " ", txtComentario.Text));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
                //limpiar();
                Botonera(true, false, false, false, false, false, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void FrmMovimientos_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false, false, false, false, false);

            mM.LlenarPartida(cmbIdP, "select idPartida from Partida", "Partida");

            actualizar();

        }

        private void dtgMovimientos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(true, false, true, true, true, true, true, true);

            fila = e.RowIndex;
            Mov.IdPartida = int.Parse(dtgMovimientos.Rows[fila].Cells[0].Value.ToString());
            Mov.IdMovimiento = int.Parse(dtgMovimientos.Rows[fila].Cells[1].Value.ToString());
            Mov.Movimiento = dtgMovimientos.Rows[fila].Cells[2].Value.ToString();
            Mov.Comentario =dtgMovimientos.Rows[fila].Cells[3].Value.ToString();
            

            cmbIdP.Text = Mov.IdPartida.ToString();
            txtIdMovimiento.Text = Mov.IdMovimiento.ToString();
            txtMovimiento.Text = Mov.Movimiento;
            txtComentario.Text = Mov.Comentario;
        }
    }
}
