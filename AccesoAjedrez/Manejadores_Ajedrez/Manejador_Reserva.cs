﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoAjedrez;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Reserva
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadesReserva reserva)
        {
            return ca.Comando(string.Format("INSERT INTO Reserva VALUES(" +
           "'{0}','{1}','{2}','{3}','{4}')",reserva.IdReserva, reserva.FechaEntrada, reserva.FechaSalida, reserva.FkIdhotel, reserva.Fkparticipante));
        }
        public string Modificar(EntidadesReserva reserva)
        {
            return ca.Comando(string.Format("UPDATE Reserva SET fechaEntrada='{0}', " + "fechaSalida='{1}' where idReserva='{2}'", reserva.FechaEntrada, reserva.FechaSalida, reserva.IdReserva));
        }
        public string EliminarDatos(EntidadesReserva reserva)
        {

            return ca.Comando(string.Format("delete from Reserva where idReserva='{0}'", reserva.IdReserva));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public void LlenarReserva(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idHotel";
        }
        public void LlenarParticipante(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "num_Socio";
        }
    }
}
