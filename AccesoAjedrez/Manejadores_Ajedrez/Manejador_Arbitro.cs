﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoAjedrez;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Arbitro
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadArbitro arbitro)
        {
            return ca.Comando(string.Format("INSERT INTO Arbitro VALUES(" +
           "{0})", arbitro.IdArbitro));

        }
        public string EliminarDatos(EntidadArbitro arbitro)
        {
            return ca.Comando(string.Format("delete from Arbitro where idArbitro='{0}'", arbitro.IdArbitro));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public void LlenarArbitro(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "num_Socio";
        }
    }
}
