﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Manejadores_Ajedrez;
using ProyectoAjedrez;

namespace PresentacionAjedrez
{
    public partial class FrmReserva : Form
    {
        
        EntidadesReserva Res = new EntidadesReserva(0, "", "", 0, 0);
        Manejador_Reserva mr;
        string t;
        int fila = 0;
        public FrmReserva()
        {
            InitializeComponent();
            mr = new Manejador_Reserva();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Botonera(false, true, false, false, true, true, true, true, true);
        }
            private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                Botonera(false, true, false, false, true, true, true, true, true);
                string rs = mr.GuardarDatos(new EntidadesReserva(0, dtfechEn.Text, 
                    dtfechSa.Text, int.Parse(cmbfkhotel.Text), int.Parse(cmbfkiparti.Text)));
                //Close();
                MessageBox.Show("Se ha guardado correctamente");
                actualizar();
                //limpiar();
                Botonera(false, true, false, false, true, true, true, true, true);
            }
            catch (Exception)
            {
                MessageBox.Show("Error Verifica los datos que ingresaste");
            }
        }
        private void Botonera(Boolean nuevo, Boolean agregar, Boolean eliminar, Boolean modificar, 
            Boolean idreserva, Boolean fehcentrada, Boolean fechasalida, Boolean fkhotel, Boolean fkparticipante)
        {
            btnNuevo.Enabled = nuevo;
            btnAgregar.Enabled = agregar;
            btnEliminar.Enabled = eliminar;
            btnModificar.Enabled = modificar;
            txtIdr.Enabled = idreserva;
            dtfechEn.Enabled = fehcentrada;
            dtfechSa.Enabled = fechasalida;
            cmbfkhotel.Enabled = fkhotel;
            cmbfkiparti.Enabled = fkparticipante;


        }
        void actualizar()
        {
            dtgReserva.DataSource = mr.Listado(string.Format("" + "select * from Reserva"), "Reserva").Tables[0];
            dtgReserva.AutoResizeColumns();
            dtgReserva.Columns[0].ReadOnly = true;

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult rs = MessageBox.Show("Atencion, ¿esta seguro de borrar la Reserva seleccionada?",
             "Atencion!!!", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                t = mr.EliminarDatos(Res);
                actualizar();
                //limpiar();
                Botonera(true, false, false, false, false, false, false, false, false);

            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rs = mr.Modificar(new EntidadesReserva(Res.IdReserva, dtfechEn.Text, dtfechSa.Text, 0, 0));
                MessageBox.Show("Se Actualizo Correctamente");
                actualizar();
                //limpiar();
                Botonera(true, false, false, false, false, false, false, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Error");

            }
        }

        private void FrmReserva_Load(object sender, EventArgs e)
        {
            Botonera(true, false, false, false, false, false, false, false, false);
            mr.LlenarParticipante(cmbfkiparti, "select Num_Socio from participantes", "participantes");
            mr.LlenarReserva(cmbfkhotel, "select idHotel from Hotel","Hotel");

            dtfechEn.Format = DateTimePickerFormat.Custom;
            dtfechEn.CustomFormat = "yyyy-MM-dd";

            dtfechSa.Format = DateTimePickerFormat.Custom;
            dtfechSa.CustomFormat = "yyyy-MM-dd";
            actualizar();
        }

        private void dtgReserva_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Botonera(true, false, true, true, true, true, true, true, true);

            fila = e.RowIndex;
            Res.IdReserva = int.Parse(dtgReserva.Rows[fila].Cells[0].Value.ToString());
            Res.FechaEntrada = dtgReserva.Rows[fila].Cells[1].Value.ToString();
            Res.FechaSalida =dtgReserva.Rows[fila].Cells[2].Value.ToString();
            Res.FkIdhotel = int.Parse(dtgReserva.Rows[fila].Cells[3].Value.ToString());
            Res.Fkparticipante = int.Parse(dtgReserva.Rows[fila].Cells[4].Value.ToString());
            
            txtIdr.Text = Res.IdReserva.ToString();
            cmbfkhotel.Text = Res.FkIdhotel.ToString();
            dtfechEn.Text = Res.FechaEntrada;
            dtfechSa.Text = Res.FechaSalida;
            cmbfkhotel.Text = Res.FkIdhotel.ToString();
            cmbfkiparti.Text = Res.Fkparticipante.ToString();
            
        }
    }
}
