﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoAjedrez
{
    public class EntidadesPais
    {
        public int Idpais { get; set; }
        public string Nombre { get; set; }
        public string NumerodeClub { get; set; }
        public string Representa { get; set; }
        public EntidadesPais(int idpais,string nombre, string numerodeclub,string representa)
        {
            Idpais = idpais;
            Nombre = nombre;
            NumerodeClub = numerodeclub;
            Representa = representa;

        }
    }
}
