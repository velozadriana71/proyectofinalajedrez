﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoAjedrez
{
    public class EntidadesCampeonato
    {
        public int IdCampeonato { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public int Fkidparticipante { get; set; }
        public EntidadesCampeonato(int idcampeonato,string nombre,string tipo,int fkparticipante)
        {
            IdCampeonato = idcampeonato;
            Nombre = nombre;
            Tipo = tipo;
            Fkidparticipante = fkparticipante;

        }

    }
}
